import { Injectable } from '@angular/core';
import { BehaviorSubject } from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class SelectionModeService {

  selectionMode$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  selectedAll$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  someChange$: BehaviorSubject<string> = new BehaviorSubject<string>('default');

  totalRecords$: BehaviorSubject<number> = new BehaviorSubject<number>(0);

  constructor() { }
}
